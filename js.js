Query('#fader img:gt(0)').hide();

setInterval(function(){
    jQuery('#fader :first-child')
        .fadeTo(500, 1)
        .next('img')
        .fadeTo(500, 2)
        .next('img')
        .fadeTo(500, 3)
        .end()
        .appendTo('#fader');
}, 5000);



